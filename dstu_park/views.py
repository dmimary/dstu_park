from django.db.models import Q
from django.shortcuts import render
from rest_framework.viewsets import ModelViewSet, ReadOnlyModelViewSet
from django.views.generic import TemplateView
from django.contrib.auth import get_user_model

from dstu_park.models import Sight
from dstu_park.serializers import SightSerializer, UserSerializer
from dstu_park.permission import IsOwner

from url_filter.integrations.drf import DjangoFilterBackend


class IndexPageView(TemplateView):
    template_name = 'index.html'


class UserViewSet(ReadOnlyModelViewSet):
    queryset = get_user_model().objects.all()
    serializer_class = UserSerializer


class SightFilter(DjangoFilterBackend):
    def filter_queryset(self, request, queryset, view):
        if not len(request.query_params):
            return queryset
        q_object_str = ''
        for key in request.query_params:
            q_object_str += f'Q({key}="{request.query_params[key]}")|'
        q_object_str = q_object_str[:-1]

        # queryset.filter(Q(title__icontains='marker')|Q(description__icontains='marker'))
        return queryset.filter(eval(q_object_str))


class SightViewSet(ModelViewSet):
    """
    List all code sights, or create a new snippet.
    """
    queryset = Sight.objects.all()
    serializer_class = SightSerializer
    permission_classes = (IsOwner,)
    filter_backends = [SightFilter]


def view_home(request):
    return render(request, 'index.html', {
        'is_not_auth': not request.user.is_authenticated,
    })
