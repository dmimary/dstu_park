# Generated by Django 2.1.7 on 2019-03-06 10:00

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('dstu_park', '0003_sight_owner'),
    ]

    operations = [
        migrations.RenameField(
            model_name='sight',
            old_name='name',
            new_name='hash_tag',
        ),
        migrations.AlterField(
            model_name='sight',
            name='owner',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='sights', to=settings.AUTH_USER_MODEL),
        ),
    ]
