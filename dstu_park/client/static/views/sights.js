import { View, CollectionView, Behavior } from 'backbone.marionette';
import * as _ from 'underscore';

import { SightsCollection } from '../models/sight.js';
import sightViewTemplate from '../../templates/sight_view.hbs';


class SightView extends View {

  constructor(options={}){
    _.defaults(options, {
      template: sightViewTemplate,
      className: 'sight',
      events: {
        'click': 'onClick',
      },
    })
    super(options);
    this.setClassName();
  }

  onClick() {
    this.trigger('capture:id', this.model.get('id'));
  }

  setClassName(){
    let owner_id = parseInt(this.model.get('owner').split('/')[5]);
    let window_user_pk = window.user_pk;
    let own_model_class = window_user_pk == owner_id ? 'owner' : '';
    if (own_model_class.length > 0){
      this.el.classList.add(own_model_class)
    }
  }
}

export class SightsCollectionView extends CollectionView {
  constructor(options={}){
    _.defaults(options, {
      className: 'search-results',
      childView: SightView,
      collection: new SightsCollection(),
      childViewEventPrefix: 'childview',
      childViewTriggers: {
        'capture:id': 'childview:capture:id',
      }
    })
    super(options);
  }

  onChildviewCaptureId(id) {
    this.trigger('capture:id', id);
  }
}


