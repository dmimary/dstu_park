import $ from 'jquery';
import * as _ from 'underscore';
import { View } from 'backbone.marionette';

import signup_template from '../../templates/signup_view.hbs';

export class SignUpView extends View {

  constructor(options={}){
    _.defaults(options, {
      className: 'signup',
      template: signup_template,
      events: {
        'click button.signup': 'onSignup',
        'click button.cancel': 'onCancel',
      },
    })
    super(options)
  }

  onSignup(events) {
    events.preventDefault();
    const url = '/auth/registration/';
    console.log('Sign up...');
    const formValues = {
      username: $('.signup input.input-username')[0].value,
      email: $('.signup input.input-email')[0].value,
      password1: $('.signup input.input-password')[0].value,
      password2: $('.signup input.input-password')[1].value,
    };

    $.ajax({
      url: url,
      type:'POST',
      dataType:"json",
      data: formValues,
      success : (json, data) => {
          console.log('Signup request details: ', formValues);
          this.trigger('workout:form', this);
      },
      error: (error) => {
        console.log('Error! Invalid data!');
      }
    });
  }

  onCancel(events) {
    events.preventDefault();
    this.trigger('workout:cancel:form', this);
  }
};
