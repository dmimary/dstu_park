import { View } from 'backbone.marionette';
import * as _ from 'underscore';
import $ from 'jquery';

import sight_template from '../../templates/sight_form.hbs';

export class SightFormView extends View {
  constructor(options={}) {
    _.defaults(options, {
      className: 'sight_form',
      template: sight_template,
      events: {
        'click button.create': 'onClickCreate',
        'click button.cancel': 'onClickCancel',
      },
  });
  super(options)
 }

  onClickCreate(events) {
    events.preventDefault();

    console.log('Submit...');

    const formValues = {
      title: $('.input-title')[1].value,
      description: $('.input-description')[1].value,
      hashtag: $('.input-hashtag')[1].value,
    };

    this.trigger('workout:create:form', this, formValues);
  }

  onClickCancel(events) {
    events.preventDefault();
    console.log('Cancel...');
    this.trigger('workout:cancel:form', this);
  }

}