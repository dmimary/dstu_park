import { View } from 'backbone.marionette';
import * as _ from 'underscore';
import $ from 'jquery';

import sight_edit_template from '../../templates/sight_edit_form.hbs';

export class SightEditFormView extends View {
  constructor(options={}) {
    _.defaults(options, {
      className: 'sight_edit_form',
      template: sight_edit_template,
      events: {
        'click button.save': 'onClickSave',
        'click button.cancel': 'onClickCancel',
      },
  });
  super(options);
  this.model = options['clicked_model'];
 }

  onClickSave(events) {
    events.preventDefault();

    console.log('Editing...');

    const formValues = {
      title: $('.input-title')[1].value,
      description: $('.input-description')[1].value,
      hashtag: $('.input-hashtag')[1].value,
    };

    this.trigger('workout:save:form', this, formValues);
  }

  onClickCancel(events) {
    events.preventDefault();
    console.log('Cancel...');
    this.trigger('workout:cancel:form', this);
  }

}