import $ from 'jquery';
import * as _ from 'underscore';
import { View } from 'backbone.marionette';

import login_template from '../../templates/login_view.hbs';

export class LogInView extends View {

  constructor(options={}){
    _.defaults(options, {
      className: 'login',
      template: login_template,
      events: {
        'click button.login': 'onLogin',
        'click button.cancel': 'onCancel',
      },
    })
    super(options)
  }

  onLogin(events) {
    events.preventDefault();
    const url = '/auth/login/';
    console.log('Login in...');
    const formValues = {
      username: $('.login input.input-username')[0].value,
      email: $('.login input.input-email')[0].value,
      password: $('.login input.input-password')[0].value,
    };

    $.ajax({
      url: url,
      type:'POST',
      dataType:"json",
      data: formValues,
      success : (json, data) => {
          console.log('Login request details: ', formValues);
          this.trigger('workout:form', this)
      },
      error: (error) => {
        console.log('Error! Invalid data!');
      }
    });
  }

  onCancel(events) {
    events.preventDefault();
    console.log('Cancel...');
    this.trigger('workout:cancel:form', this);
  }
}
