import { Model } from 'backbone';
import { View } from 'backbone.marionette';
import * as _ from 'underscore';

import { userModel } from '../models/user.js';
import { SightFormView } from './sight_form.js';

import contextmenu_template from '../../templates/contextmenu.hbs';

export class ContextMenu extends View {
  constructor(options={}){
    _.defaults(options, {
      template: contextmenu_template;
      regions: {
        'region_menu': 'contextmenu'
      },
      childViewTriggers: {
        'workout:form': 'child:workout:form'
        'workout:cancel:form': 'child:workout:cancel:form',
        'workout:create:form': 'child:workout:create:form',
      },
      model: new userModel(),
    });
    super(options)
  }

  onChildWorkoutCancelForm(childView) {
    childView.destroy();
  }

  modelSync(model){
    this.showChildView('region_menu', new AuthenticatedMenu());
  }

  modelError(error){
    this.showChildView('region_menu', new UnauthenticatedMenu());
  }

  onRender(){
    this.options.user_model.fetch();
    this.options.user_model.on('sync', this.modelSync, this);
    this.options.user_model.on('error', this.modelError, this);
  }
}

class AuthenticatedMenu extends View {

  constructor(options={}){
    _.defaults(options, {

    }),
    super(options)
  }

}

class UnauthenticatedMenu extends View {

  constructor(options={}){
    _.defaults(options, {

    })
    super(options)
  }

}
