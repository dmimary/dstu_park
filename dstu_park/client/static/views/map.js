import { Model, Radio } from 'backbone';
import { View } from 'backbone.marionette';
import * as _ from 'underscore';
import * as L from 'leaflet';
import $ from 'jquery';

import 'leaflet-osm';
import 'leaflet-draw';
import 'leaflet.measurecontrol';
import 'leaflet-mouse-position';

import * as grocery from '../icons/grocery.png';
import * as shop from '../icons/shop.png';
import * as club from '../icons/club.png';
import * as chinatown from '../icons/chinatown.png';
import * as cafe from '../icons/cafe.png';
import * as oasis from '../icons/oasis.png';
import * as forest from '../icons/forest.png';
import * as restaurant from '../icons/restaurant.png';
import * as embankment from '../icons/embankment.png';
import * as palace from '../icons/palace.png';
import * as zoo from '../icons/zoo.png';
import * as pagoda from '../icons/pagoda.png';
import * as pub from '../icons/pub.png';
import * as bunkBed from '../icons/bunk-bed.png';
import * as busStop from '../icons/bus-stop.png';
import * as church from '../icons/church.png';
import * as dumbbell from '../icons/dumbbell.png';
import * as gasStation from '../icons/gas-station.png';
import * as park from '../icons/park.png';
import * as owner from '../icons/owner.png';
import * as _default from '../icons/default.png';

import { SightsCollection } from '../models/sight.js';
import { SightsCollectionView } from './sights.js';
import { SightFormView } from './sight_form.js';
import { SightEditFormView } from './sight_edit_form.js';

import map_template from '../../templates/map_view.hbs';
import marker_menu_tmp from '../../templates/marker_contextmenu.hbs';
import map_menu_tmp from '../../templates/map_contextmenu.hbs';


class CustomIcon extends L.Icon {
  constructor(options){
    _.defaults(options, {
      iconSize:    [30, 30],
      popupAnchor: [0, -15],
    });
    super(options);
  }
}

export class MapView extends View {
  constructor(options={}){
    _.defaults(options, {
      id: 'map_view',
      template: map_template,
      triggers: {
        'keyup input.search-bar': 'data:entered'
      },
      regions: {
        'sightsList': '.sights-list',
        'sight_form_container': '.container',
      },
      childViewTriggers: {
        'workout:cancel:form': 'child:workout:cancel:form',
        'workout:create:form': 'child:workout:create:form',
        'workout:save:form': 'child:workout:save:form',
        'capture:id': 'child:capture:id',
      },
    });
    super(options);

    this.map = L.map('map_view').setView([51.5300, -0.1478], 13);
    new L.OSM.Mapnik().addTo(this.map);
    // Layer group of markers:
    this.markers = L.layerGroup().addTo(this.map);

    L.Control.measureControl().addTo(this.map);
    L.control.mousePosition().addTo(this.map);

    this.radio = Radio.channel('sights');
    this.radio.on('rebuildSights', this.rebuildSights, this)
  }

  onDataEntered(view, event) {
    if (event.originalEvent.keyCode != 13){
      return
    }
    let searchKey = event.target.value;
    if (searchKey.length == 0){
      this.sightsCollection.fetch();
    } else {
      this.sightsCollection.fetch({
        url: '/api/sights/?title__icontains=' + searchKey
              + '&description__icontains=' + searchKey
              + '&hash_tag__icontains=' + searchKey,
        data: {
          'title': searchKey
        }
       });
    }
  }

  onChildWorkoutCreateForm(childView, formData) {

    this.sightData = Object.assign({
      hash_tag: formData.hashtag,
      title: formData.title,
      description: formData.description,
    }, this.sightData);

    this.sightsCollection.create(this.sightData, {
      wait: true,
      success: (model, response) => {
        console.log('Model was added successfully!');
        this.render();
      },
      error: (error) => {
        alert("Oops... Please, log in!");
        console.log(error);
      }
    });
  }

  onChildWorkoutSaveForm(childView, formData) {

    this.newData = {
      hash_tag: formData.hashtag,
      title: formData.title,
      description: formData.description,
    };

    childView.model.save(this.newData, {
      url: childView.model.url() + '/',
      patch: true,
      wait: true,
      success: (model, response) => {
        console.log('Model was changed successfully!');
        this.render();
      },
      error: (error) => {
        console.log('Error!', error);
      }
    });

    console.log(childView.model);
    this.render();
  }

  onChildWorkoutCancelForm(childView) {
    childView.destroy();
    this.el.lastChild.classList.remove('display');
  }

  onRightMapClick(point) {

    const newPopup = L.popup();
    const popupInfo = '<div class="popup-info">' + point.latlng + '</div>';

    if (window.user_pk === undefined) {

      newPopup
        .setLatLng(point.latlng)
        .setContent(popupInfo)
        .openOn(this.map);
    } else {

      const contentHtml = map_menu_tmp();

      this.sightData = {
        owner: "/api/users/" + window.user_pk + "/",
        point: "POINT(" + point.latlng.lat + ' ' + point.latlng.lng + ')',
        icon: "default",
      };

      newPopup
        .setLatLng(point.latlng)
        .setContent(popupInfo + contentHtml)
        .openOn(this.map);

      $("div .popup-create-button").on('click', () => {
        this.el.lastChild.classList.add('display');
        this.map.closePopup(newPopup);
        this.showChildView('sight_form_container', new SightFormView());
      });
    }
  }

  onRightMarkerClick(point, sight_id, sight_title) {

    const ownerUrl = this.sightsCollection.get(sight_id).attributes.owner;

    function getMarkerOwner() {
      var result = "";
      $.ajax({
        url: ownerUrl,
        type: 'GET',
        async: false,
        success: (data) => {
          result =  data.username;
        },
        error: () => {
          console.log('User URL:', ownerUrl);
        }
      });
      return result;
    }

    const newPopup = L.popup();
    const markerOwner = getMarkerOwner();
    const popupInfo = '<div class="popup-info"><b>' + sight_title + '</b><br/>@' + markerOwner + '</div>';
    let marker = null;

    if (window.user_pk === parseInt(ownerUrl.split('/')[5])) {

      const contentHtml = marker_menu_tmp();

      newPopup
        .setLatLng(point.latlng)
        .setContent(popupInfo + contentHtml)
        .openOn(this.map);

      $("div .popup-delete-button").on('click', () => {

        this.sightsCollection.get(sight_id).destroy({
          wait: true,
          success: (data) => {
            this.markers.eachLayer((layer) => {
              if (layer.options.customId === sight_id) {
                this.map.removeLayer(layer);
              }
            });
            this.map.closePopup();
            console.log('Model was deleted successfully!');
          },
          error: (error) => {
            console.log("WOW! This sight is not yours!");
          }
        });
      });

      $("div .popup-edit-button").on('click', () => {
        this.el.lastChild.classList.add('display');
        this.map.closePopup(newPopup);
        this.showChildView('sight_form_container', new SightEditFormView({
          clicked_model: this.sightsCollection.get(sight_id),
        }));
      });
    } else {
      newPopup
        .setLatLng(point.latlng)
        .setContent(popupInfo + point.latlng)
        .openOn(this.map);
    }
  }

  rebuildSights(){
    if (this.sightsCollection){
      this.sightsView.render()
      this.collectionPointsSync(this.sightsView.collection);
    }
  }


  collectionPointsSync(collection) {
    this.markers.eachLayer(layer => {
      this.map.removeLayer(layer)
    })

    collection.models.forEach(sight => {

      var icon = null;

      if (parseInt(sight.get('owner').split('/')[5]) === window.user_pk && window.user_pk !== 1) {
        icon = owner;
      } else {
        switch(sight.get('icon')) {
          case 'park':
            icon = park;
            break;
          case 'bunk-bed':
            icon = bunkBed;
            break;
          case 'bus-stop':
            icon = busStop;
            break;
          case 'dumbbell':
            icon = dumbbell;
            break;
          case 'church':
            icon = church;
            break;
          case 'gas-station':
            icon = gasStation;
            break;
          case 'shop':
            icon = shop;
            break;
          case 'grocery':
            icon = grocery;
            break;
          case 'club':
            icon = club;
            break;
          case 'chinatown':
            icon = chinatown;
            break;
          case 'cafe':
            icon = cafe;
            break;
          case 'oasis':
            icon = oasis;
            break;
          case 'forest':
            icon = forest;
            break;
          case 'restaurant':
            icon = restaurant;
            break;
          case 'embankment':
            icon = embankment;
            break;
          case 'palace':
            icon = palace;
            break;
          case 'zoo':
            icon = zoo;
            break;
          case 'pagoda':
            icon = pagoda;
            break;
          case 'pub':
            icon = pub;
            break;
          default:
            icon = _default;
            break;
        }
      }

      const customIcon = new CustomIcon({ iconUrl: icon });

      L.marker(sight.get('point').coordinates, {
          icon: customIcon,
          customId: sight.id,
        })
        .addTo(this.markers)
        .bindPopup(sight.get('title'))
        .on('contextmenu', (point) => {
          this.onRightMarkerClick(point, sight.id, sight.get('title'));
        });
    });
  }

  onChildCaptureId(id) {
    let marker = null;

    this.markers.eachLayer((layer) => {
      if (layer.options.customId === id) {
        marker = layer;
      }
    });

    this.map.flyTo(marker.getLatLng(), 19);

    this.map.once('moveend zoomend', () => {
      this.map.openPopup(marker.getPopup(), marker.getLatLng());
    });
  }

  onRender() {

    this.map.on('contextmenu', (point) => {

      this.onRightMapClick(point);
    });

    if (this.sightsCollection) {
      this.sightsCollection.unbind('sync');
    }

    this.sightsView = new SightsCollectionView();

    this.sightsView.collection.on('sync', this.collectionPointsSync, this);

    this.sightsView.collection.fetch({
      success: (collection) => {
        this.showChildView('sightsList', this.sightsView);
        this.sightsCollection = this.sightsView.collection;
      },
      error: (error) => {
        console.log(error);
      }
    })
  }
};
