import { Radio } from 'backbone';
import { View } from 'backbone.marionette';
import * as _ from 'underscore';

import { userModel } from '../models/user.js';
import { SignUpView } from './signup.js';
import { LogInView } from './login.js';
import { LogOutView } from './logout.js';

import panel_template from '../../templates/settings_panel.hbs';
import auth_panel_template from '../../templates/auth_panel_template.hbs';
import unauth_panel_template from '../../templates/unauth_panel_template.hbs';

export class SettingsPanel extends View {
  constructor(options={}){
    _.defaults(options, {
      template: panel_template,
      regions: {
        'region_panel': '.settings_panel'
      },
      model: userModel,
      childViewTriggers: {
        'workout:form': 'child:workout:form',
      },
    });
    super(options);
    this.radio = Radio.channel('sights');
  }

  onChildWorkoutForm(childView){
    this.render();
  }

  // event handler
  modelSync(model){
    window.user_pk = model.get('pk');
    window.username = model.get('username');
    this.showChildView('region_panel', new AuthenticatedPanel({
       model: model,
    }));
    this.radio.trigger('rebuildSights');
  }

  modelError(error){
    window.user_pk = undefined;
    this.showChildView('region_panel', new UnauthenticatedPanel());
    this.radio.trigger('rebuildSights');
  }

  onRender(){
    this.model.fetch({
      success: data => {
        this.modelSync(data);
      },
      error: error => {
        this.modelError(error);
      }
    });
  }
}

class AuthenticatedPanel extends View {

  constructor(options={}){
    _.defaults(options, {
      template: auth_panel_template,
      events: {
        'click .tologout': 'onClickLogout',
      },
      regions: {
        'logout_container': '.container',
      },
      childViewTriggers: {
        'workout:form': 'workout:form',
        'workout:cancel:form': 'child:workout:cancel:form',
      },
    }),
    super(options);

  }

  onChildWorkoutCancelForm(childView){
    childView.destroy();
    this.el.lastChild.classList.remove('display');
  }

  onClickLogout(){
    this.el.lastChild.classList.add('display');
    this.showChildView('logout_container', new LogOutView())
  }
}

class UnauthenticatedPanel extends View {

  constructor(options={}){
    _.defaults(options, {
      template: unauth_panel_template,
      events: {
        'click .tosignup': 'onClickSignup',
        'click .tologin': 'onClickLogin',
      },
      regions: {
        'signup_container': '.container',
        'login_container': '.container',
      },
      childViewTriggers: {
        'workout:form': 'workout:form',
        'workout:cancel:form': 'child:workout:cancel:form',
      },
    })
    super(options)
  }

  onChildWorkoutCancelForm(childView){
    childView.destroy();
    this.el.lastChild.classList.remove('display');
  }

  onClickSignup(){
    this.el.lastChild.classList.add('display');
    this.showChildView('signup_container', new SignUpView());
  }

  onClickLogin(){
    this.el.lastChild.classList.add('display');
    this.showChildView('login_container', new LogInView());
  }
}