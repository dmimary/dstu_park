import $ from 'jquery';
import * as _ from 'underscore';
import { View } from 'backbone.marionette';

import logout_template from '../../templates/logout_view.hbs';

export class LogOutView extends View {

  constructor(options={}){
    _.defaults(options, {
      className: 'logout',
      template: logout_template,
      events: {
        'click button.logout': 'onLogout',
        'click button.cancel': 'onCancel',
      },
    })
    super(options)
  }

  onLogout(events) {
    events.preventDefault();
    const URL = '/auth/logout/';
    console.log('Logout...');
    $.ajax({
      url: URL,
      method:'POST',
      dataType:"json",
      success: (json) => {
          console.log('Logout request successed');
          this.trigger('workout:form', this);

      },
      error: (error) => {
        console.log('Error! Logout.');
      }
    });
  }

  onCancel(events) {
    events.preventDefault();
    console.log('Cancel...');
    this.trigger('workout:cancel:form', this);
  }
}