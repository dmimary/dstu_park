import { Collection } from 'backbone';
import * as _ from 'underscore';


export class SightsCollection extends Collection {
  url(){
    return '/api/sights/';
  }
}