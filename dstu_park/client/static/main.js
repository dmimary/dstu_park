import { setRenderer } from 'backbone.marionette';
import * as Bb from 'backbone';
import * as Tc from 'marionette.templatecache';
import { View } from 'backbone.marionette';
import * as _ from 'underscore';
import { Radio } from 'backbone';

import './main.css';
import * as CSRF from './csrf_token.js';

import { App } from './apps/app.js';
import { MapView } from './views/map.js';
import { SettingsPanel } from './views/settings_panel.js';

import main_view_template from '../templates/main_view.hbs';

document.addEventListener("DOMContentLoaded", () => {
  new App().start();
});

export class MainView extends View {
    constructor(options={}){
      _.defaults(options, {
        id: 'main_view',
        template: main_view_template,
        regions: {
          'main_container': '.main_container',
          'settings_container': '.settings_container',
        }
      })
      super(options);
    }

    onRender(){
      const mapView = new MapView();
      const settingsPanel = new SettingsPanel();

      this.showChildView('settings_container', settingsPanel);

      settingsPanel.options.model.on('all', function(model){
          this.showChildView('main_container', mapView);
      }, this);
    }
}
