from django.contrib.gis.db import models
from django.contrib.auth import get_user_model


class Sight(models.Model):
    # Configurations for leaflet markers-sights
    hash_tag = models.SlugField(max_length=50, blank=True)
    title = models.CharField(max_length=100)
    description = models.CharField(max_length=200, blank=True)
    icon = models.SlugField(max_length=50, default='default')
    point = models.PointField(geography=True)
    owner = models.ForeignKey(get_user_model(), on_delete=models.CASCADE, related_name='sights')

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['-id']
