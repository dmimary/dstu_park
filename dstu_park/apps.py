from django.apps import AppConfig


class DstuParkConfig(AppConfig):
    name = 'dstu_park'
