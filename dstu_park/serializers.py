from django.contrib.auth import get_user_model
from rest_framework.serializers import ModelSerializer, HyperlinkedRelatedField

from dstu_park.models import Sight


class UserSerializer(ModelSerializer):

    class Meta:
        model = get_user_model()
        fields = ('username', 'email')
        ordering = ('username',)


class SightSerializer(ModelSerializer):
    """ A class to serialize locations as GeoJSON compatible data """
    owner = HyperlinkedRelatedField(
        view_name='user-detail',
        queryset=get_user_model().objects.all(),
    )

    class Meta:
        model = Sight
        fields = ('id', 'hash_tag', 'title', 'description', 'icon', 'point', 'owner')
        ordering = ('id',)
